#include <stdio.h>
#include "archivo.h"

int main(){
	char *str ="1995-04-13";
	char *puntero = str;
	desglosar(puntero);

	int lista[]={1,2,3,4};
	printf("ubicacion del elemento 3 en el array [");
	for(int i =0; i<4 ; i=i+1){
		printf(" %d ",lista[i]);
	}
	printf("]\n");
	primeraOcurrencia(lista,3);
	
	int a = 3;
	int b = 2;
	printf("%d %d \n",a,b);
	intercambiar(&a,&b);
	printf("%d %d \n",a,b);
}
